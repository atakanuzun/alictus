import pandas as pd

def calculate():
    #create the headers
    header_l = ['orderkey','partkey','suppkey','linenumber','quantity','extendedprice','discount','tax',
    'returnflag','linestatus','shipdate','commitdate','receiptdate','shipinstruct','shipmode','comment']
    header_o = ['orderkey','custkey','orderstatus','totalprice','orderdate','orderpriority','clerk',
    'shippriority','comment']
    header_c = ['custkey', 'name','address','nationkey','phone','acctbal','mktsegment','comment']

    #read tables
    lineitem = pd.read_table('lineitem.tbl', delimiter = '|', names = header_l, index_col=False)
    orders = pd.read_table('orders.tbl', delimiter = '|', names = header_o, index_col=False) 
    customer = pd.read_table('customer.tbl', delimiter = '|', names = header_c, index_col=False)

    #read segments
    segments = pd.read_csv('query_example.txt', header=None)

    #join tables
    join = pd.merge(lineitem[['orderkey','extendedprice','discount']]
    ,orders[['orderkey','custkey']], on='orderkey')
    join2 = pd.merge(join, customer[['custkey','mktsegment']], on='custkey')

    #filter by segment name and print average value
    for x in range(len(segments)):
        result = join2[(join2.mktsegment == segments[0][x])]
        avg = (result["extendedprice"] * (1-result["discount"])).mean()
        print("{:10.3f}".format(avg))

calculate()
